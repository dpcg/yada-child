<?php
function sp_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		array(
			'name'   => 'Adminbar Link Comments to Pending',
			'slug'   => 'adminbar-link-comments-to-pending',
			'source' => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
		),

		array(
			'name'               => 'WordPress Importer',
			'slug'               => 'wordpress-importer',
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
		),

        array(
			'name'               => 'Disable Emojis',
			'slug'               => 'disable-emojis',
			'required'           => true,
			'force_activation'   => true,
			'force_deactivation' => false,
		),
		
        array(
			'name'               => 'Edit Flow',
			'slug'               => 'edit-flow',
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
		),
		
		array(
			'name'               => 'Limit Login Attempts',
			'slug'               => 'limit-login-attempts',
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
		),

		array(
			'name'               => 'Google Analytics for WordPress by MonsterInsights',
			'slug'               => 'google-analytics-for-wordpress',
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
		),

		array(
			'name'               => 'WPBakery Visual Composer',
			// The plugin name
			'slug'               => 'js_composer',
			// The plugin slug (typically the folder name)
			'source'             => get_stylesheet_directory() . '/lib/plugins/js_composer.zip',
			// The plugin source
			'required'           => true,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '5.2',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => false,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url'       => 'http://bit.ly/WPBakery-WordPress',
			// If set, overrides default API URL and points to an external URL
		),

		array(
			'name'               => 'Massive Addons For WPBakery',
			// The plugin name
			'slug'               => 'massive_addons',
			// The plugin slug (typically the folder name)
			'source'             => get_stylesheet_directory() . '/lib/plugins/massive-addons-wpbakery.zip',
			// The plugin source
			'required'           => true,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '2.3.2',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => false,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url'       => 'http://bit.ly/ma-addons',
			// If set, overrides default API URL and points to an external URL
		),

		array(
			'name'               => 'Generate Press Premium',
			// The plugin name
			'slug'               => 'generatepress_premium',
			// The plugin slug (typically the folder name)
			'source'             => get_stylesheet_directory() . '/lib/plugins/gp-premium.zip',
			// The plugin source
			'required'           => true,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '1.5.6',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => true,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url'       => 'https://generatepress.com/?ref=667',
			// If set, overrides default API URL and points to an external URL
		),
		
		array(
			'name'               => 'Advanced Custom Fields Pro',
			// The plugin name
			'slug'               => 'advanced-custom-fields-pro',
			// The plugin slug (typically the folder name)
			'source'             => get_stylesheet_directory() . '/lib/plugins/acfpro.zip',
			// The plugin source
			'required'           => true,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '5.6.5',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => true,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
		),

		array(
			'name'               => 'Auto Terms of Service and Privacy Policy',
			'slug'               => 'auto-terms-of-service-and-privacy-policy',
			'required'           => false,
			'force_activation'   => false,
			'force_deactivation' => false,
		),


	);

	$config = array(
		'id'           => 'yada',
		// Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',
		// Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins',
		// Menu slug.
		'parent_slug'  => 'themes.php',
		// Parent menu slug.
		'capability'   => 'edit_theme_options',
		// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,
		// Show admin notices or not.
		'dismissable'  => true,
		// If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',
		// If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,
		// Automatically activate plugins after installation or not.
		'message'      => '',
		// Message to output right before the plugins table.
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'schemapress' ),
			'menu_title'                      => __( 'Install Plugins', 'schemapress' ),
			/* translators: %s: plugin name. */
			'installing'                      => __( 'Installing Plugin: %s', 'schemapress' ),
			/* translators: %s: plugin name. */
			'updating'                        => __( 'Updating Plugin: %s', 'schemapress' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'schemapress' ),
			'notice_can_install_required'     => _n_noop(
			/* translators: 1: plugin name(s). */
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'yada'
			),
			'notice_can_install_recommended'  => _n_noop(
			/* translators: 1: plugin name(s). */
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'yada'
			),
			'notice_ask_to_update'            => _n_noop(
			/* translators: 1: plugin name(s). */
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'yada'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
			/* translators: 1: plugin name(s). */
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'yada'
			),
			'notice_can_activate_required'    => _n_noop(
			/* translators: 1: plugin name(s). */
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'yada'
			),
			'notice_can_activate_recommended' => _n_noop(
			/* translators: 1: plugin name(s). */
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'yada'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'yada'
			),
			'update_link'                     => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'yada'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'yada'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'yada' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'yada' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'yada' ),
			/* translators: 1: plugin name. */
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'yada' ),
			/* translators: 1: plugin name. */
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'yada' ),
			/* translators: 1: dashboard link. */
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'yada' ),
			'dismiss'                         => __( 'Dismiss this notice', 'yada' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'yada' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'yada' ),

			'nag_type' => 'update-nag',
			// Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
	);

	tgmpa( $plugins, $config );
	
};
add_action( 'tgmpa_register', 'sp_register_required_plugins' );
