# Yada Child Theme

Yada Child Theme for GeneratePress is a fast, lightweight (less than 1MB zipped), mobile responsive WordPress theme built with speed, SEO and usability in mind.

## Description

Yada Child Theme can serve as a solid base for any website, and works great with any of your favorite page builders. With an emphasis on WordPress coding standards, we ensure GeneratePress is compatible with all well-coded plugins, including major ones like WooCommerce, WPML, BuddyPress and bbPress.

Yada is device friendly (mobile and tablet), uses 100% valid HTML, is fully schema microdata integrated, is translated into over 20 languages by our amazing community and is cross browser compatible (IE9+).

Some of our features include 9 widget areas, 5 navigation positions, 5 sidebar layouts, dropdown menus (click or hover) and a back to top button.

All our options use the native WordPress Customizer, meaning you can see every change you make instantly before pressing the publish button.

## Installation

### From within WordPress
1. Visit "Appearance > Themes > Add New"
2. Search for "Yada Child Theme"
3. Install and activate

## Frequently Asked Questions

### Is Yada Child Theme Free?
No. Yada Child theme is commercial open sourced. The parent GeneratePress theme is free.

#### Does GeneratePress WordPress theme have a pro version?
It does! GeneratePress has a premium plugin which extends the available options in the theme. You can learn more about [GP Premium](http://bit.ly/wordpress-generatepress).


### Where can I find documentation?
GeneratePress has extensive documentation you can find [here](https://docs.generatepress.com).

### Do you offer support?
Definitely. We offer support for the free theme in the [WordPress.org forums](https://wordpress.org/support/theme/generatepress).

Premium customers have access to our very own [support forum](https://generatepress.com/support).

We try to answer all questions - free or premium - within 24 hours.

### Where can I find the theme options?
All of our options can be found in the Customizer in 'Appearance > Customize'.

### Does GeneratePress have any widget areas?
GeneratePress has up to 9 widget areas which you can add widgets to in Appearance > Widgets.

### How can I make my site look like your screenshot?
If you want to replicate the screenshot you see on WordPress.org, please refer to [this article](https://docs.generatepress.com/article/replicating-the-screenshot/).
