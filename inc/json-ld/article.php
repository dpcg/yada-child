<?php
// Initialise this action
add_action('wp_head', 'json_ld_article');

function json_ld_article()
{
  // Only on single pages
  if ( is_page_template( 'page-templates/article.php' ) )
  {
    // Set POST data into memory
    global $post;
    setup_postdata($post);
    // Variables
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $logo = wp_prepare_attachment_for_js($custom_logo_id);
    $image = get_post_thumbnail_id(($post->ID), 'full');
    $attachment_meta = wp_prepare_attachment_for_js($image);
    $excerpt = get_the_excerpt();
    $words = str_word_count(strip_tags($post->post_content), 0);

    // Open script
    $html = '<script type="application/ld+json">'. "\r\n";

    $html .= '{';
        $html .= '"@context": "https://schema.org",';
        $html .= '"@type": "Article",';
          $html .= '"image": {';
            $html .= '"@type": "ImageObject",';
            $html .= '"name": "' .$attachment_meta[ 'alt' ] . '",';
            $html .= '"url": "' . $attachment_meta[ 'url'] . '",';
            $html .= '"height": "' . $attachment_meta[ 'height' ] . '",';
            $html .= '"width": "' . $attachment_meta[ 'width' ] . '",';
            $html .= '"description": "' .$attachment_meta[ 'description' ] . '"';
            $html .= '},';
        $html .= '"headline": "' . get_the_title() . '",';
        $html .= '"datePublished": "' . get_the_date('c') . '",';
        $html .= '"dateModified": "' . get_the_modified_date('c') . '",';
        $html .= '"wordCount": "' . $words . '",';

        $html .= '"mainEntityOfPage": {';
            $html .= '"@type":"ItemPage",';
            $html .= '"@id": "' . get_the_permalink() . '",';
            $html .= '"potentialAction": {';
            $html .= '"@type": "SearchAction",';
            $html .= '"target": "' . site_url() .'/search?q={search_term_string}",';
            $html .= '"query-input": "required name=search_term_string"';
            $html .= '},';
            $html .= '"name": "' . get_the_title() . '",';
            $html .= '"keywords": "'. comma_tags( get_the_tags(), false ) . '",';
            $html .= '"mentions": ['.comma_tags( get_the_tags(), true ).'],';
            $html .= '"breadcrumb": {';
            echo $html;
             echo wp_schema_breadcrumbs();
            $html = '';
    if ( $image )
            $html .= '"image": {';
                $html .= '"@type": "ImageObject",';
                    $html .= '"url": "' . $attachment_meta[ 'url' ] . '",';
                    $html .= '"height": "' . $attachment_meta[ 'height' ] . '",';
                    $html .= '"width": "' . $attachment_meta[ 'width' ] . '",';
                    $html .= '"name": "' .$attachment_meta[ 'alt' ] . '",';
                    $html .= '"description": "' .$attachment_meta[ 'description' ] . '",';
                    $html .= '"representativeOfPage": "true"';
                $html .= '},';
            $html .= '"lastReviewed": "' . get_the_modified_date('c') . '",';
            $html .= '"reviewedBy": {';
            $html .= '"@type": "Person",';
                $html .= '"name": "' . get_the_author() . '"';
            $html .= '}';
         $html .= '},';

        $html .= '"author": {';
            $html .= '"@type": "Person",';
            $html .= '"name": "' . get_the_author() . '"';
        $html .= '},';

        $html .= '"publisher": {';
          $html .= '"@type": "Organization",';
          $html .= '"name": "' . get_bloginfo('name') . '",';
          $html .= '"logo": {';
            $html .= '"@type": "ImageObject",';
            $html .= '"url": "' . $logo[ 'url' ] . '",';
            $html .= '"height": "' . $logo[ 'height' ] . '",';
            $html .= '"width": "' . $logo['width'] . '",';
            $html .= '"name": "' . $logo[ 'alt' ] . '",';
            $html .= '"description": "' .$logo[ 'description' ] . '"';
          $html .= '}';
    $html .= '}';
 $html .= '}';

    // Close script
    $html .= '</script>'. "\r\n";

    echo $html;
  }
}
