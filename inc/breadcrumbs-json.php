<?php
/*
This function is written to work only
in the Yada Child Theme for GeneratePress
*/

function wp_schema_breadcrumbs()
{
    global $post;
    setup_postdata($post);
    $post_id = get_option('page_for_posts');
    $post = get_post($post_id);
    $slug = $post->post_name;
    $blog_posts_page_slug = '/'.$slug;
    $site_name = get_bloginfo('blogname');

    $breadcrumb = array(
        '@context'        => 'http://schema.org',
        '@type'           => 'BreadcrumbList',
        'itemListElement' => array(),
    );

    if (is_singular('post')) {
        // if on a single blog post
        $breadcrumb['itemListElement'][] = array(
            '@type'    => 'ListItem',
            'position' => 1,
            'item'     => array(
                '@id'  => get_site_url('url').$blog_posts_page_slug,
                'name' => $site_name,
            ),
        );
        $breadcrumb['itemListElement'][] = array(
            '@type'    => 'ListItem',
            'position' => 2,
            'item'     => array(
                '@id'  => get_permalink(),
                'name' => get_the_title(),
            ),
        );
    } elseif (is_singular('product')) {
        // if on a single product page
        global $post;
        $terms = wp_get_object_terms($post->ID, 'product_cat');
        if (!is_wp_error($terms)) {
            $product_category_slug = $terms[0]->slug;
            $product_category_name = $terms[0]->name;
            $breadcrumb['itemlistElement'][] = array(
                '@type'    => 'ListItem',
                'position' => 1,
                'item'     => array(
                    '@id'  => get_bloginfo('url').'/products/'.$product_category_slug.'/',
                    'name' => $product_category_name,
                ),
            );
            $breadcrumb['itemlistElement'][] = array(
                '@type'    => 'ListItem',
                'position' => 2,
                'item'     => array(
                    '@id'  => get_permalink(),
                    'name' => get_the_title(),
                ),
            );
        }
    } elseif (is_page() && !is_front_page()) { // if on a regular WP Page
        global $post;
        if (is_page() && $post->post_parent) { // if is a child page
            $post_data = get_post($post->post_parent);
            $parent_page_slug = $post_data->post_name;
            $parent_page_url = get_bloginfo('url').'/'.$parent_page_slug.'/';
            $parent_page_title = ucfirst($parent_page_slug);
            $position_number = '2';
            $breadcrumb['itemlistElement'][] = array(
                '@type'    => 'ListItem',
                'position' => 1,
                'item'     => array(
                    '@id'  => $parent_page_url,
                    'name' => $parent_page_title,
                ),
            );
            $breadcrumb['itemlistElement'][] = array(
                '@type'    => 'ListItem',
                'position' => $position_number,
                'item'     => array(
                    '@id'  => get_permalink(),
                    'name' => get_the_title(),
                ),
            );
        }
    } elseif (is_home()) { // if on the blog page
        $breadcrumb['itemlistElement'][] = array(
            '@type'    => 'ListItem',
            'position' => 1,
            'item'     => array(
                'id'   => get_site_url('url').$blog_posts_page_slug,
                'name' => '$site_name',
            ),
        );
    } elseif (is_category() || is_tag()) {
        $breadcrumb['itemlistElement'][] = array(
            '@type'    => 'ListItem',
            'position' => 1,
            'item'     => array(
                '@id'  => get_site_url('url').$blog_posts_page_slug,
                'name' => '$site_name',
            ),
        );
        $breadcrumb['itemListElement'][] = array(
            '@type'    => 'ListItem',
            'position' => 2,
            'item'     => array(
                '@id'  => 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
                'name' => (is_category() ? single_cat_title('', false) : single_tag_title('', false)),
            ),
        );
    } elseif (is_tax('product_cat') || is_tax('product_tag')) { // product category and taxonomy pages
        global $post;
        $termname = get_query_var('term');
        $termname = ucfirst($termname);
        $breadcrumb['itemlistElement'] = array(
            '@type'    => 'ListItem',
            'position' => 1,
            'item'     => array(
                '@id'  => get_permalink(woocommerce_get_page_id('shop')),
                'name' => 'shop',
            ),
        );
        $breadcrumb['ItemlistElement'][] = array(
            '@type'    => 'ListItem',
            'position' => 2,
            'item'     => array(
                '@id'  => 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
                'name' => $termname,
            ),
        );
    } elseif (is_archive()) { // date based archives and a catch all for the rest
        $breadcrumb['itemlistElement'] = array(
            '@type'    => 'ListItem',
            'position' => 1,
            'item'     => array(
                '@id'  => get_site_url('url').$blog_posts_page_slug,
                'name' => $site_name,
            ),
        );
        $breadcrumb['itemlistElement'][] = array(
            '@type'    => 'ListItem',
            'position' => 2,
            'item'     => array(
                '@id'  => 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
                'name' => 'Archives',
            ),
        );
    } else {
        $breadcrumb['itemlistElement'] = array(
            '@type'    => 'ListItem',
            'position' => 1,
            'item'     => array(
                '@id'  => 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
                'name' => 'Page',
            ),
        );
    };
    $breadcrumb = json_encode($breadcrumb, JSON_FORCE_OBJECT) . "\n\n";
}