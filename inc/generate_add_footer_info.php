<?php
if ( ! function_exists( 'generate_add_footer_info' ) ) {
	add_action( 'generate_credits', 'generate_add_footer_info' );
	/**
	 * Add the copyright to the footer
	 *
	 * @since 0.1
	 */
	function generate_add_footer_info() {
		$copyright = '<span class="copyright">&copy; ' . date( 'Y' ).'</span> &bull; <a href="' . esc_url( home_url( '/', 'https')) .'" target="_blank">'. bloginfo('name').'</a>';

		echo apply_filters( 'generate_copyright', $copyright ); // WPCS: XSS ok.
	}
}
