<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package GeneratePress
 * @subpackage Yada
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="inside-article">
		<?php
		/**
		 * generate_before_content hook.
		 *
		 * @since 0.1
		 *
		 * @hooked generate_featured_page_header_inside_single - 10
		 */
		do_action( 'generate_before_content' );

		if ( generate_show_title() ) : ?>

			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

		<?php endif;

		/**
		 * generate_after_entry_header hook.
		 *
		 * @since 0.1
		 *
		 * @hooked generate_post_image - 10
		 */
		do_action( 'generate_after_entry_header' );
		?>

		<div class="entry-content">
			<?php
			the_content();

			wp_link_pages( array(
				'before' => '<aside><nav class="page-links">' . __( 'Pages:', 'generatepress' ),
				'after'  => '</nav></aside>',
			) );
			?>
		</div>
		<!-- .entry-content -->
// If comments are open or we have at least one comment, load up the comment template.
		<?	if ( comments_open() || '0' != get_comments_number() ) : ?>

		<section class="comments-area">
			<?php comments_template( '/template-parts/comments.php' ); ?>
		</section>

		<?php endif;

		/**
		 * generate_after_content hook.
		 *
		 * @since 0.1
		 */
		do_action( 'generate_after_content' );
		?>
	</div><!-- .inside-article -->
</article><!-- #post-## -->